/*
============================================================================
Filename    : implementation.cu
Author      : Matthieu De Beule, Ulysse Widmer
SCIPER      : 269623, 269583
============================================================================
*/

#include <iostream>
#include <iomanip>
#include <sys/time.h>
#include <cuda_runtime.h>
using namespace std;

// CPU Baseline
void array_process(double *input, double *output, int length, int iterations)
{
    double *temp;

    for(int n=0; n<(int) iterations; n++)
    {
        for(int i=1; i<length-1; i++)
        {
            for(int j=1; j<length-1; j++)
            {
                output[(i)*(length)+(j)] = (input[(i-1)*(length)+(j-1)] +
                                            input[(i-1)*(length)+(j)]   +
                                            input[(i-1)*(length)+(j+1)] +
                                            input[(i)*(length)+(j-1)]   +
                                            input[(i)*(length)+(j)]     +
                                            input[(i)*(length)+(j+1)]   +
                                            input[(i+1)*(length)+(j-1)] +
                                            input[(i+1)*(length)+(j)]   +
                                            input[(i+1)*(length)+(j+1)] ) / 9;

            }
        }
        output[(length/2-1)*length+(length/2-1)] = 1000;
        output[(length/2)*length+(length/2-1)]   = 1000;
        output[(length/2-1)*length+(length/2)]   = 1000;
        output[(length/2)*length+(length/2)]     = 1000;

        temp = input;
        input = output;
        output = temp;
    }
}

__global__ void iteration_on_gpu(double *input, double *output, int length){
    int i = blockIdx.x + 1;
    int j = threadIdx.x + 1;
    output[(i)*(length)+(j)] = (input[(i-1)*(length)+(j-1)] +
                                input[(i-1)*(length)+(j)]   +
                                input[(i-1)*(length)+(j+1)] +
                                input[(i)*(length)+(j-1)]   +
                                input[(i)*(length)+(j)]     +
                                input[(i)*(length)+(j+1)]   +
                                input[(i+1)*(length)+(j-1)] +
                                input[(i+1)*(length)+(j)]   +
                                input[(i+1)*(length)+(j+1)] ) / 9;

    output[(length/2-1)*length+(length/2-1)] = 1000;
    output[(length/2)*length+(length/2-1)]   = 1000;
    output[(length/2-1)*length+(length/2)]   = 1000;
    output[(length/2)*length+(length/2)]     = 1000;
}

// GPU Optimized function
void GPU_array_process(double *input, double *output, int length, int iterations)
{
    //Cuda events for calculating elapsed time
    cudaEvent_t cpy_H2D_start, cpy_H2D_end, comp_start, comp_end, cpy_D2H_start, cpy_D2H_end;
    cudaEventCreate(&cpy_H2D_start);
    cudaEventCreate(&cpy_H2D_end);
    cudaEventCreate(&cpy_D2H_start);
    cudaEventCreate(&cpy_D2H_end);
    cudaEventCreate(&comp_start);
    cudaEventCreate(&comp_end);

    /* Preprocessing */
    double *temp;
    double *input_gpu;
    double *output_gpu;
    size_t array_size = sizeof(double) * length * length;
    cudaMalloc((void**)&input_gpu, array_size);
    cudaMalloc((void**)&output_gpu, array_size);
    /* ---- */

    cudaEventRecord(cpy_H2D_start);

    /* Copying array from host to device */
    cudaMemcpy(input_gpu, input, array_size, cudaMemcpyHostToDevice);
    cudaMemcpy(output_gpu, output, array_size, cudaMemcpyHostToDevice);
    /* ---- */

    cudaEventRecord(cpy_H2D_end);
    cudaEventSynchronize(cpy_H2D_end);

    //Copy array from host to device
    cudaEventRecord(comp_start);

    size_t nbr_of_blocks = length - 2;
    size_t threads_per_block = length - 2;

    /* GPU calculation */
    for(int i(0); i < iterations; i++){
        iteration_on_gpu<<<nbr_of_blocks, threads_per_block>>>(input_gpu, output_gpu, length);
        temp = input_gpu;
        input_gpu = output_gpu;
        output_gpu = temp;
    }
    /* ---- */

    cudaEventRecord(comp_end);
    cudaEventSynchronize(comp_end);

    cudaEventRecord(cpy_D2H_start);
    /* Copying array from device to host */
    if(iterations % 2 == 0){
        temp = input_gpu;
        input_gpu = output_gpu;
        output_gpu = temp;
    }

    cudaMemcpy(output, output_gpu, array_size, cudaMemcpyDeviceToHost);
    /* ---- */
    cudaEventRecord(cpy_D2H_end);
    cudaEventSynchronize(cpy_D2H_end);

    /* Postprocessing */
    cudaFree(input_gpu);
    cudaFree(output_gpu);
    /* ---- */

    float time;
    cudaEventElapsedTime(&time, cpy_H2D_start, cpy_H2D_end);
    cout<<"Host to Device MemCpy takes "<<setprecision(4)<<time/1000<<"s"<<endl;

    cudaEventElapsedTime(&time, comp_start, comp_end);
    cout<<"Computation takes "<<setprecision(4)<<time/1000<<"s"<<endl;

    cudaEventElapsedTime(&time, cpy_D2H_start, cpy_D2H_end);
    cout<<"Device to Host MemCpy takes "<<setprecision(4)<<time/1000<<"s"<<endl;
}

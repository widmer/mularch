/*
============================================================================
Filename    : algorithm.c
Author      : Matthieu De Beule, Ulysse Widmer
SCIPER		: 269623, 269583

============================================================================
*/
#include <math.h>
#include <omp.h>
#define INPUT(I,J) input[(I)*length+(J)]
#define OUTPUT(I,J) output[(I)*length+(J)]

void simulate(double *input, double *output, int threads, int length, int iterations)
{
    double *temp;
    omp_set_num_threads(threads);

    #pragma omp parallel
    {
        for(int n=0; n < iterations; n++)
        {
            #pragma omp for schedule(guided)
            for(int i=1; i<length-1; i++)
            {
                for(int j=1; j<length-1; j++)
                {
                    if ( ((i == length/2-1) || (i== length/2))
                    && ((j == length/2-1) || (j == length/2)) )
                    continue;

                    OUTPUT(i,j) = (INPUT(i-1,j-1) + INPUT(i-1,j) + INPUT(i-1,j+1) +
                    INPUT(i,j-1)   + INPUT(i,j)   + INPUT(i,j+1)   +
                    INPUT(i+1,j-1) + INPUT(i+1,j) + INPUT(i+1,j+1) )/9;
                }
            }
            //Make sure this is only executed once and only after all threads are done with this iteration
            #pragma omp barrier
            #pragma omp single
            {
                temp = input;
                input = output;
                output = temp;
            }
        }
    }
}

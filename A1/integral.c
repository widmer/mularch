/*
============================================================================
Filename    : pi.c
Author      : Matthieu De Beule, Ulysse Widmer
SCIPER		: 269623, 269583
============================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include "utility.h"
#include "function.c"

double integrate (int num_threads, int samples, int a, int b, double (*f)(double));

int main (int argc, const char *argv[]) {

    int num_threads, num_samples, a, b;
    double integral;

    if (argc != 5) {
		printf("Invalid input! Usage: ./integral <num_threads> <num_samples> <a> <b>\n");
		return 1;
	} else {
        num_threads = atoi(argv[1]);
        num_samples = atoi(argv[2]);
        a = atoi(argv[3]);
        b = atoi(argv[4]);
	}

    set_clock();

    /* You can use your self-defined funtions by replacing identity_f. */
    integral = integrate (num_threads, num_samples, a, b, identity_f);

    printf("- Using %d threads: integral on [%d,%d] = %.15g computed in %.4gs.\n", num_threads, a, b, integral, elapsed_time());

    return 0;
}


double integrate (int num_threads, int samples, int a, int b, double (*f)(double)) {
    double integral = 0, f_x_sum = 0, x;
    unsigned long i;
    omp_set_num_threads(num_threads);

    #pragma omp parallel private(x)
    {
        rand_gen generator = init_rand();
        #pragma omp for reduction(+:f_x_sum)
        for (i = 0; i < samples; i++){
            x = a + (b-a)*next_rand(generator);
            f_x_sum += f(x);
        }
        free_rand(generator);
    }
    integral = (f_x_sum/samples) * (b-a);

    return integral;
}

/*
============================================================================
Filename    : pi.c
Author      : Matthieu De Beule, Ulysse Widmer
SCIPER		: 269623, 269583
============================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include "utility.h"

double calculate_pi (int num_threads, int samples);

int main (int argc, const char *argv[]) {

    int num_threads, num_samples;
    double pi;

    if (argc != 3) {
		printf("Invalid input! Usage: ./pi <num_threads> <num_samples> \n");
		return 1;
	} else {
        num_threads = atoi(argv[1]);
        num_samples = atoi(argv[2]);
	}

    set_clock();
    pi = calculate_pi (num_threads, num_samples);

    printf("- Using %d threads: pi = %.15g computed in %.4gs.\n", num_threads, pi, elapsed_time());

    return 0;
}


double calculate_pi (int num_threads, int samples) {
    double pi;
    int hits = 0;
    unsigned long i;
    double randomX, randomY;
    omp_set_num_threads(num_threads);
    #pragma omp parallel private(randomX, randomY)
    {
        rand_gen generator = init_rand();
        #pragma omp for reduction(+:hits)
        for (i = 0; i < samples; i++){
            randomY = next_rand(generator);
            randomX = next_rand(generator);
            if (randomX*randomX + randomY*randomY <= 1)
                hits++;
        }
        free_rand(generator);
    }
    pi = 4.*hits/samples;

    return pi;
}
